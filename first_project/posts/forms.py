from .models import Order, Cost
from django.forms import ModelForm , TextInput, Select

class Cost(ModelForm):
    class Meta:
        
        model = Cost
        fields = ['TypeOfWork', 'HowBig']

        widgets = {
            'TypeOfWork': Select(attrs={
                'class': 'choose_type-of-job inputs', 
            }),

            'HowBig': Select(attrs={
                'class': 'choose_how-hard inputs',
                'placeholder': 'Выберите сложность'
            }),

            # 'cost': TextInput(attrs={
            #     'class': 'cost_value texts_about-values',
            # })
        }

class Order(ModelForm):
    
    class Meta:
        model = Order
        fields = ['name', 'mail', 'service', 'comment']

        widgets = {
            'name': TextInput(attrs={
                'class': 'buy_defult_inputs',
                'placeholder': 'Имя'
            }),
            
            'mail': TextInput(attrs={
                'class': 'buy_defult_inputs',
                'placeholder': 'Почта'
            }),
            
            'service': TextInput(attrs={
                'class': 'buy_defult_inputs',
                'placeholder': 'Услуга'
            }),
            
            'comment': TextInput(attrs={
                'class': 'buy_big_input',
                'placeholder': 'Комментарий'
            })
        }