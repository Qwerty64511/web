from django.contrib import admin
from .models import FirstClass, Order, Cost

admin.site.register(FirstClass)
admin.site.register(Order)
admin.site.register(Cost)

# Register your models here.
