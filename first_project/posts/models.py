from django.db import models

class FirstClass(models.Model):
    costs = models.CharField(max_length=100, default="")
    texts = models.CharField(max_length=1000, default="")
    nameOfservice = models.CharField(max_length=1000, default="")


class Cost(models.Model):
    TELEGRAMM = 'Telegramm'
    WEB = 'web'
    TECH_DIRECTION = 'Tech'
    From_scratch = 'from_scratch'
    modification = 'mod'
    fast_and_hard = 'fast_hard'

    Cost_services = [
        (TELEGRAMM, 'telegram-bot'),
        (WEB, 'Web-site'),
        (TECH_DIRECTION, 'Tech-direction')
    ]

    Cost_complexity = [
        (From_scratch, 'с нуля'),
        (modification, 'Доработка готового'),
        (fast_and_hard, 'Быстро и с нуля')
    ]

    TypeOfWork = models.CharField(max_length=100, choices=Cost_services, default=TELEGRAMM)
    HowBig = models.CharField(max_length=100, choices=Cost_complexity, default=From_scratch)

    cost =  models.CharField(max_length=100, default='')

class Order(models.Model):
        name = models.CharField(max_length=100, default="")
        mail = models.CharField(max_length=100, default="")
        service = models.CharField(max_length=100, default="")
        comment = models.CharField(max_length=100, default="")
# Create your models here.
