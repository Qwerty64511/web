from django.shortcuts import render
from .models import FirstClass, Cost
import sqlite3

from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import Order, Cost

# def new_order(name, email, service, comment):
#     name.create()

#     email.create()

#     service.create()

#     comment.create()

def get_order(request):
    error = ''

    services = FirstClass.objects.all()
    
    form_result = Cost(request.POST)

    if request.method == 'POST':
        form_result = Cost(request.POST)

        if form_result.is_valid():
            
            data = form_result.cleaned_data
            
            job = data['TypeOfWork']
            complexity = data['HowBig']

            TELEGRAMM = 'Telegramm'
            WEB = 'web'
            TECH_DIRECTION = 'Tech'
            From_scratch = 'from_scratch'
            modification = 'mod'
            fast_and_hard = 'fast_hard'

            costs = {
                TELEGRAMM: 2,
                WEB: 4,
                TECH_DIRECTION: 7,
                From_scratch: 5,
                modification: 3,
                fast_and_hard: 8,
            }

            points = costs[job] + costs[complexity]

            if points >= 5:
               final_cost = 100000
            
            if points >= 10:
                final_cost = 200000
            
            if points >= 15:
                final_cost = 150000
            
            
        else:
            error = 'form not filled in'

    if request.method == 'POST':
        form = Order(request.POST)
        
        if form.is_valid():
            form.save()
        
        else:
            error = 'form not filled in'

    form = Order()

    countOfObj = len(services)
    if countOfObj % 5 == 0:
        countOfObj = 5
    
    if countOfObj % 2 == 0:
        countOfObj = 2
    

    data = {
        
        'form': form,
        'form_result': form_result,
        'errror': error,
        'services': services,
        'servicesCount': countOfObj,
    }
    return render(request, 'index.html', data)

# def index(request):
#     services = FirstClass.objects.all()

#     return render(request, 'index.html',
#     {'services': services,
#     })
    
def index2(request):
    objects = FirstClass.objects.all()
    return render(request, 'index2.html', {'obj': objects})

# Create your views here.
