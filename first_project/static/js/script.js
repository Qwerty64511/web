// // переходим к самому коду, который нам будет нужен

// const box = document.querySelector('#box') // При помощи данной команды мы ищем определённый эллемент. querrySelector() - в скобки задаётся 
// // идентификатор по которому мы ищем / класс по которому мы ищем. Короче задаём условие поиска(селектор)
// // Результат выведет в терминал наш код html-а и по нему можно уже найти нужный эллемент
// const wrapper = document.querySelector('.wrapper') 
// const oneCircle = wrapper.querySelector('circle') //во врапере ищем эллемент circle. Выведет первый эллемент 
// // т.к. эта штука ищет сверху вниз и когда находит первый останавливается

// const circles = document.querySelectorAll('circle') // Ищем все эллементы circle. circles - псевдомассив(т.е. набор каких-то эллементов, 
// // которые не ...(проебланил)).  Разница - можно узнать длину массива, преобразовать в строку и тп. Но с псевдомассивами так делать нельзя

// circles.forEach((item) => {
//     item.style.backgroundColor = 'black'
// }) // В функции создали подфунцию, которая задаёт определённый цвет задника

// box.style.backgroundColor = 'blue' // задали box стиль задника blue
// box.style.width = '500px' // ззадали боксу ширину 500px

// box.style.cssText = 'background-color: blue; width: 500px'

// for (let i = 0; i < circles.length; i++) // let i = 0 задали локальную переменную i = 0, i < circles.length - задаём условие работы пока i < 
// // circles.length, i++ - увеличиваем i на один после каждой иттерации
// {
//     circles[i].style.backgroundColor = 'green'
// }

// const div = document.createElement('div') // создаём эллемент div
// div.innerHTML = "I'm innerHTML" // Вставляем что-то внутрь созданного div-a.
// div.innerHTML = "<h2>H2 header</h2>" // - теперь внутрь дива засунулся рабочий эллемент 
// // создали какой-то новый класс в css

// // Зачем вообще мы всё это делаем. Чтобы можно было перезаписывать при помощи js какие-то эллементы. 
// // Короче если тебе надо будет помнять стили/вёрстку
// // Арзитектурно правильно и масштабируемо 

// div.classList.add('red') // создался div с классом red и внутри него h2)
// if (div.classList.contains('red')) { // проверяем на наличие в диве класса red
//     console.log('Have red')
// }

// div.classList.remove('red') // удаляем класс red
// div.classList.toggle('red') // 
// div.append(div) // Добавляет наш эллемент в какого-то родителя в конец
// wrapper.prepend('red') // то же, что и выше, но вставляем в начало(ну как я понял)
// oneCircle.before('div') // вставляем до первого circle
// oneCircle.after('div') // вставляем после первого circle
// oneCircle.remove() // удаляем oneCircle
// div.textContent = 'Freontend is the best' // Вставляем в div какой-то текст(могу ошибаться)
// div.textContent = '<h2></h2>' // выведет <h2></h2> текстом
// // Слушатель событий

// button.addEventListener('click', (e) => {
//     e.target.classList.toggle('black') // прои клике на кнопку, она меняет цвет на чёрный(видимо). e = event - переменная события.
//     // black - класс на который меняется
// })

const buttonAll = document.querySelector('.button_all');
const textColorAll = document.querySelector('.all_text');

const buttonOrder = document.querySelector('.button_order');
const textColorOrder = document.querySelector('.order_text');

const buttonDevelopment = document.querySelector('.button_development');
const textDevelopment = document.querySelector('.development_text');

const buttonDesign = document.querySelector('.button_design');
const textDesign = document.querySelector('.design_text');

const buttonOrderCalculate = document.querySelector('.order__calculate');
const frameCalculateFinal = document.querySelector('.calculate__finally');

const plus1 = document.querySelector('.plus_1');
const questionBased = document.querySelector('.question_based');

const questionDeadlines = document.querySelector('.question_deadlines');
const plus2 = document.querySelector('.plus_2');

const questionOrder = document.querySelector('.question_order');
const plus3 = document.querySelector('.plus_3');

const questionFixes = document.querySelector('.question_fixes');
const plus4 = document.querySelector('.plus_4');

const questionProjects = document.querySelector('.question_projects');
const plus5 = document.querySelector('.plus_5');

const questionTilda = document.querySelector('.question_tilda');
const plus6 = document.querySelector('.plus_6');
const tildaText = document.querySelector('.tilda_text');

const listTypeOfJob = document.querySelector('.list__Type_of_job')
const checkMark = document.querySelector('.check-mark')

plus6.addEventListener('click', (e) => {
    questionTilda.classList.toggle('question_opend')
    tildaText.classList.toggle('invisible')
});

plus5.addEventListener('click', (e) => {
    questionProjects.classList.toggle('question_opend')
});

plus4.addEventListener('click', (e) => {
    questionFixes.classList.toggle('question_opend')
});

plus3.addEventListener('click', (e) => {
    questionOrder.classList.toggle('question_opend');
});

plus2.addEventListener('click', (e) => {
    questionDeadlines.classList.toggle('question_opend');
});

plus1.addEventListener('click', (e) => {
    questionBased.classList.toggle('question_opend');
});


var type = ''
var complexity = ''
var check = ''

const form = document.getElementById('forms__cost');

const SpanFirst = document.querySelector('#span_first');
const SpanSecond = document.querySelector('#span_second');

form.addEventListener('submit', function(e) {

    e.preventDefault()
    
    var type = form.querySelector('[name="TypeOfWork"]');
    var complexity = form.querySelector('[name="HowBig"]');
    var check = document.getElementById('checkbox').checked;

    const data = {
        type: type.value,
        complexity: complexity.value
    };
    
    type = data['type'];
    complexity = data['complexity'];

    if (check == true && type != '' && complexity != '') {
        frameCalculateFinal.classList.toggle('calculate__final')    
    }

    SpanFirst.innerHTML = data.type;
    SpanSecond.innerHTML = data.complexity;



});

        

const buttons = document.querySelectorAll('.navigation__buttons__flex');
const photos = document.querySelectorAll('.photos');

// const photosOrder = document.querySelectorAll()
buttons.forEach((element) => {

    element.addEventListener('click', (e) => {
        buttons.forEach(function(item){
            item.classList.add('frames_white');
            item.classList.remove('frames_black');
            item.classList.remove('all_text');
            item.classList.add('color_black');

        });

        const currentCategory = element.dataset.filter

        filter(currentCategory, photos);

        function filter (category, items) {
            items.forEach(item => {
                const isFiltred = (item.dataset.filter == category);
                
                if (isFiltred) {
                    item.classList.remove('invisible');
                };

                if (!isFiltred) {
                    item.classList.add('invisible');
                };
                
                if (category == 'all') {
                    item.classList.remove('invisible');
                };
            });
        };

        e.target.classList.toggle('frames_black');
        e.target.classList.toggle('frames_white');
        e.target.classList.toggle('color_black');
        e.target.classList.toggle('all_text');
        
    });

    // e.add('all_text')

})
// Через forEach прохожусь по всем нужным кнопкам, убираю класс frames_black. Потом уже добавляю frames_black нужной
// buttonAll.addEventListener('click', (e) => {
//     buttonAll.classList.toggle('frames_white');
//     buttonAll.classList.toggle('frames_black');

//     textColorAll.classList.toggle('color_black');

//     });

// buttonOrder.addEventListener('click', (e) => {
//     buttonOrder.classList.toggle("frames_white");
//     buttonOrder.classList.toggle('frames_black');

//     textColorOrder.classList.toggle('all_text');
// });

// buttonDevelopment.addEventListener('click', (e) => {
//     buttonDevelopment.classList.toggle("frames_white");
//     buttonDevelopment.classList.toggle('frames_black');
//     textDevelopment.classList.toggle('all_text');
// });

// buttonDesign.addEventListener('click', (e) => {
//     buttonDesign.classList.toggle("frames_white");
//     buttonDesign.classList.toggle('frames_black');
//     textDesign.classList.toggle('all_text');
// });

